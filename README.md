# Cryptopals

Solutions to the [cryptopals crypto challenges].  You may want to look
at the [notes].  Some files produce interesting text when run, others
just test assertions.  A few have to be run in parallel, sometimes
with environment variables set.  The Emacs Lisp solutions require
Emacs 27.1 or newer with GnuTLS and bignum support.

[cryptopals crypto challenges]: https://www.cryptopals.com/
[notes]: notes.md
