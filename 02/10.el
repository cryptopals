(load (expand-file-name "../util" (file-name-directory (or load-file-name buffer-file-name))))

(setq ciphertext
      (base64-to-bytes
       (file-contents (expand-file-name "10.txt" (file-name-directory (or load-file-name buffer-file-name))))))
(setq key "YELLOW SUBMARINE")
(setq iv (make-string aes-128-iv-size 0))

(message "%s" (aes-128-cbc-decrypt ciphertext key iv))
