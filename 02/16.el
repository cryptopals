(load (expand-file-name "../util" (file-name-directory (or load-file-name buffer-file-name))))

(setq key (random-bytes aes-128-key-size))
(setq iv (random-bytes aes-128-iv-size))

(defun user-data-to-query-string (user-data)
  (setq user-data (replace-regexp-in-string ";" "%3B" user-data))
  (setq user-data (replace-regexp-in-string "=" "%3D" user-data))
  (let ((plaintext (concat "comment1=cooking%20MCs;userdata="
                           user-data
                           ";comment2=%20like%20a%20pound%20of%20bacon")))
    (aes-128-cbc-encrypt (pkcs7pad plaintext) key iv)))

(defun verify-query-string (ciphertext)
  (let* ((query-string (pkcs7unpad (aes-128-cbc-decrypt ciphertext key iv)))
         (kvs (mapcar (lambda (kv) (split-string kv "="))
                      (split-string query-string ";"))))
    (message "XXX: %S" query-string)
    (equal (cadr (assoc "admin" kvs)) "true")))

;; |comment1=cooking|%20MCs;userdata=|AAAAAAAAAAAAAAAA|:admin-true;comm|ent2=%20like%20a|%20pound%20of%20|bacon
;; |comment1=cooking|%20MCs;userdata=|!?????!?????????|;admin=true;comm|ent2=%20like%20a|%20pound%20of%20|bacon
(setq ciphertext (user-data-to-query-string "AAAAAAAAAAAAAAAA:admin-true"))
(aset ciphertext 32 (logxor (aref ciphertext 32) (logxor ?: ?\;)))
(aset ciphertext 38 (logxor (aref ciphertext 38) (logxor ?- ?\=)))
(message "Admin: %s" (verify-query-string ciphertext))
