(load (expand-file-name "../util" (file-name-directory (or load-file-name buffer-file-name))))
(require 'seq)

(defun encryption-oracle (input)
  (let* ((prefix (random-bytes (random-integer 5 11)))
         (suffix (random-bytes (random-integer 5 11)))
         (plaintext (pkcs7pad (concat prefix input suffix)))
         (key (random-bytes aes-128-key-size))
         (iv (random-bytes aes-128-iv-size)))
    (if (zerop (random-integer 0 2))
        (progn
          (message "Using ECB")
          (aes-128-ecb-encrypt plaintext key))
      (message "Using CBC")
      (aes-128-cbc-encrypt plaintext key iv))))

(defun detect-cipher-mode ()
  (let ((input (make-string (* aes-128-block-size 3) ?A)))
    (if (repeated-block-p (seq-partition (encryption-oracle input)
                                         aes-128-block-size))
        (message "Detected ECB")
      (message "Detected CBC"))))

(detect-cipher-mode)
