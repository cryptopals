(load (expand-file-name "../util" (file-name-directory (or load-file-name buffer-file-name))))

(setq unpadded "YELLOW SUBMARINE")
(setq padded "YELLOW SUBMARINE\x04\x04\x04\x04")

(test-equal (pkcs7pad unpadded 20) padded)
