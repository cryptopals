(load (expand-file-name "../util" (file-name-directory (or load-file-name buffer-file-name))))

(test-equal (pkcs7unpad "ICE ICE BABY\x04\x04\x04\x04") "ICE ICE BABY")
(test-error (pkcs7unpad "ICE ICE BABY\x05\x05\x05\x05"))
(test-error (pkcs7unpad "ICE ICE BABY\x01\x02\x03\x04"))
