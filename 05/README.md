# Crypto Challenge Set 5

This is the first set of **number-theoretic cryptography** challenges,
and also our coverage of message authentication.

This set is **significantly harder** than the last set. The concepts
are new, the attacks bear no resemblance to those of the previous
sets, and... math.

On the other hand, **our favorite cryptanalytic attack ever** is in
this set (you'll see it soon). We're happy with this set. Don't wimp
out here. You're almost done!

33. [Implement Diffie-Hellman](https://depp.brause.cc/cryptopals/05/33.md)
34. [Implement a MITM key-fixing attack on Diffie-Hellman with parameter injection](https://depp.brause.cc/cryptopals/05/34.md)
35. [Implement DH with negotiated groups, and break with malicious "g" parameters](https://depp.brause.cc/cryptopals/05/35.md)
36. [Implement Secure Remote Password (SRP)](https://depp.brause.cc/cryptopals/05/36.md)
37. [Break SRP with a zero key](https://depp.brause.cc/cryptopals/05/37.md)
38. [Offline dictionary attack on simplified SRP](https://depp.brause.cc/cryptopals/05/38.md)
39. [Implement RSA](https://depp.brause.cc/cryptopals/05/39.md)
40. [Implement an E=3 RSA Broadcast attack](https://depp.brause.cc/cryptopals/05/40.md)
