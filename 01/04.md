# Detect single-character XOR

One of the 60-character strings in [this file](https://depp.brause.cc/cryptopals/01/04.txt) has been encrypted
by single-character XOR.

Find it.

(Your code from #3 should help.)
