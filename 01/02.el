(load (expand-file-name "../util" (file-name-directory (or load-file-name buffer-file-name))))

(setq input (hex-to-bytes "1c0111001f010100061a024b53535009181c"))
(setq key (hex-to-bytes "686974207468652062756c6c277320657965"))
(setq output "746865206b696420646f6e277420706c6179")

(test-equal (bytes-to-hex (xor-bytes input key)) output)
