(load (expand-file-name "../util" (file-name-directory (or load-file-name buffer-file-name))))
(require 'seq)

(setq ciphertexts
      (mapcar 'hex-to-bytes
       (file-lines (expand-file-name "08.txt" (file-name-directory (or load-file-name buffer-file-name))))))

(defun repeated-block-p (blocks)
  (/= (length blocks) (length (seq-uniq blocks))))

(setq ecb-ciphertext (seq-find (lambda (ciphertext)
                                 (repeated-block-p
                                  (seq-partition ciphertext
                                                 aes-128-block-size)))
                               ciphertexts))

(test ecb-ciphertext)
(message "Repeated block detected in %S"
         (mapcar 'bytes-to-hex (seq-partition ecb-ciphertext
                                              aes-128-block-size)))
