(load (expand-file-name "../util" (file-name-directory (or load-file-name buffer-file-name))))

(setq input "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d")
(setq output "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t")

(test-equal (bytes-to-base64 (hex-to-bytes input)) output)
