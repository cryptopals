(load (expand-file-name "../util" (file-name-directory (or load-file-name buffer-file-name))))

(setq ciphertext (hex-to-bytes "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"))

(let ((best-score 0)
      (best-byte 0))
  (dotimes (i 256)
    (let ((score (english-score (xor-byte ciphertext i))))
      (if (> score best-score)
          (setq best-score score
                best-byte i))))
  (message "best score: %.2f" best-score)
  (message "plaintext: %s" (xor-byte ciphertext best-byte)))
