(load (expand-file-name "../util" (file-name-directory (or load-file-name buffer-file-name))))
(require 'seq)

(setq ciphertexts (mapcar 'hex-to-bytes (file-lines (expand-file-name "04.txt" (file-name-directory (or load-file-name buffer-file-name))))))

(defun detect-best-score (bytes)
  (let ((best-score 0)
        (best-byte 0))
    (dotimes (i 256)
      (let ((score (english-score (xor-byte ciphertext i))))
        (if (> score best-score)
            (setq best-score score
                  best-byte i))))
    (list best-score best-byte)))

(let ((best-plaintext nil)
      (best-score 0))
  (dolist (ciphertext ciphertexts)
    (seq-let (score byte) (detect-best-score ciphertext)
      (when (> score best-score)
        (setq best-score score
              best-plaintext (xor-byte ciphertext byte)))))
  (message "Best score: %.2f" best-score)
  (message "Best plaintext: %s" best-plaintext))
