(load (expand-file-name "../util" (file-name-directory (or load-file-name buffer-file-name))))

(setq ciphertext
      (base64-to-bytes
       (file-contents (expand-file-name "07.txt" (file-name-directory (or load-file-name buffer-file-name))))))
(setq key "YELLOW SUBMARINE")

(message "%s" (aes-128-ecb-decrypt ciphertext key))
