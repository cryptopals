# Crypto Challenge Set 8

This is **the second of two sets we generated after the original 6**.

This set focuses on abstract algebra, including DH, GCM, and (most
importantly) elliptic curve cryptography. Fair warning - it's really
tough! There's a ton of content here, and it's more demanding than
anything we've released so far. By the time you're done, you will have
written an ad hoc, informally-specified, bug-ridden, slow
implementation of one percent of SageMath.

57. [Diffie-Hellman Revisited: Small Subgroup Confinement](https://depp.brause.cc/cryptopals/08/57.md)
58. [Pollard's Method for Catching Kangaroos](https://depp.brause.cc/cryptopals/08/58.md)
59. [Elliptic Curve Diffie-Hellman and Invalid-Curve Attacks](https://depp.brause.cc/cryptopals/08/59.md)
60. [Single-Coordinate Ladders and Insecure Twists](https://depp.brause.cc/cryptopals/08/60.md)
61. [Duplicate-Signature Key Selection in ECDSA (and RSA)](https://depp.brause.cc/cryptopals/08/61.md)
62. [Key-Recovery Attacks on ECDSA with Biased Nonces](https://depp.brause.cc/cryptopals/08/62.md)
63. [Key-Recovery Attacks on GCM with Repeated Nonces](https://depp.brause.cc/cryptopals/08/63.md)
64. [Key-Recovery Attacks on GCM with a Truncated MAC](https://depp.brause.cc/cryptopals/08/64.md)
65. [Truncated-MAC GCM Revisited: Improving the Key-Recovery Attack via Ciphertext Length Extension](https://depp.brause.cc/cryptopals/08/65.md)
66. [Exploiting Implementation Errors in Diffie-Hellman](https://depp.brause.cc/cryptopals/08/66.md)
