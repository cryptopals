# Crypto Challenge Set 3

This is the next set of **block cipher cryptography challenges** (even
the randomness stuff here plays into block cipher crypto).

This set is **moderately difficult**. It includes a famous attack
against CBC mode, and a "cloning" attack on a popular RNG that can be
annoying to get right.

We've also reached a point in the crypto challenges where all the
challenges, with one possible exception, are valuable in breaking
real-world crypto.

17. [The CBC padding oracle](https://depp.brause.cc/cryptopals/03/17.md)
18. [Implement CTR, the stream cipher mode](https://depp.brause.cc/cryptopals/03/18.md)
19. [Break fixed-nonce CTR mode using substitutions](https://depp.brause.cc/cryptopals/03/19.md)
20. [Break fixed-nonce CTR statistically](https://depp.brause.cc/cryptopals/03/20.md)
21. [Implement the MT19937 Mersenne Twister RNG](https://depp.brause.cc/cryptopals/03/21.md)
22. [Crack an MT19937 seed](https://depp.brause.cc/cryptopals/03/22.md)
23. [Clone an MT19937 RNG from its output](https://depp.brause.cc/cryptopals/03/23.md)
24. [Create the MT19937 stream cipher and break it](https://depp.brause.cc/cryptopals/03/24.md)
